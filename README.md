# learning tower

![exploded assembly (FreeCAD) and in use (real life)](pictures/other/explodedAssembly_and_inUse.png "Left: Exploded assembly (FreeCAD) | right: In use (real life)")

A learning tower, which allows children to reach for example the height of the worktop and help in the kitchen. Or to reach the sink and brush their teeth like the adults.

The learning tower can be assembled in three different configurations:
1. Low standing height: 300 mm
2. Middle standing height: 400 mm
3. High standing height: 500 mm

The learning tower is made out of multiplex birch (ger: Multiplex Birke) with a thickness of 18 mm. I've oiled the surface and used Osmo Hartwachs-Öl Original 3032 Clear Satin [Ger.: Farblos Seidenmatt] for this (applied using a foam roller). To close cracks and holes in the wood, I've used wood filler from clou (color tone spruce).

In order to store the learning tower, it can be easily disassembled into its individual parts. The parts are mainly plates with a thickness of 18 to 36 mm (sidepanels with sidepanel bars).

Designed in [FreeCAD](https://www.freecadweb.org/) v0.19 (last used version is 0.19.24276).

The learning tower I've build is in use since May 2020. My daughter loves it!

## Structure of the FreeCAD files
* **[`models/learning_tower_components_FC0.19.FCStd`](models/learning_tower_components_FC0.19.FCStd)**: All single components and the corresponding [TechDraw Pages](https://wiki.freecadweb.org/TechDraw_Module#Pages)
* **[`models/learning_tower_assembly_FC0.19.FCStd`](models/learning_tower_assembly_FC0.19.FCStd)**: The full assembly, all subassemblies, the BOM and the corresponding [TechDraw Pages](https://wiki.freecadweb.org/TechDraw_Module#Pages). The components are linked into this file using [App Link](https://wiki.freecadweb.org/App_Link)
* **[`models/learning_tower_assembly_Part-SimpleCopy_FC0.19.FCStd`](models/learning_tower_assembly_Part-SimpleCopy_FC0.19.FCStd)**: Copies of the full assembly in all three configurations and the exploded assembly using [Part SimpleCopy](https://wiki.freecadweb.org/Part_SimpleCopy) and the corresponding [TechDraw Pages](https://wiki.freecadweb.org/TechDraw_Module#Pages). This file also includes the BOM for the overview drawings. The BOM from **[`models/learning_tower_assembly_FC0.19.FCStd`](models/learning_tower_assembly_FC0.19.FCStd)** is linked into this file using [App Link](https://wiki.freecadweb.org/App_Link)

## Power- and "special" tools I've used (sorted alphabetically)
1. belt sander
2. cordless screwdriver
3. jigsaw
4. random orbital sander (only for a very short time)
5. router
6. table drilling machine
7. [Universal Dowelmaster Set from wolfcraft](https://www.wolfcraft.de/products/wolfcraft/en/EUR/Products/Wood-joints/Dowel-jointers/Universal-Dowelmaster-Set/p/P_4645)
8. vibratory sander

## Workbenches I've used (sorted alphabetically)
1. [Assembly3](https://wiki.freecadweb.org/Assembly3_Workbench)
2. [Draft](https://wiki.freecadweb.org/Draft_Module)
3. [Part](https://wiki.freecadweb.org/Part_Module)
4. [PartDesign](https://wiki.freecadweb.org/PartDesign_Workbench)
5. [Spreadsheet](https://wiki.freecadweb.org/Spreadsheet_Workbench)
6. [TechDraw](https://wiki.freecadweb.org/TechDraw_Workbench)

## Things I'd do differently next time
Regarding FreeCAD:
1. Get into [Sketcher CarbonCopy](https://wiki.freecadweb.org/Sketcher_CarbonCopy), [clone](https://wiki.freecadweb.org/PartDesign_Clone) and [ShapeBinders](https://wiki.freecadweb.org/PartDesign_ShapeBinder) instead of recreating sketches for referencing purposes (I've done this because of little experience with these tools and fear of the [topological naming problem](https://wiki.freecadweb.org/Topological_naming_problem))
2. For complex/hard to calculate technical drawings:
   - Export SVG from TechDraw pages in an early stage and use (for example) Inkscape to finish the drawing. Example: Drawing of the full assembly with a lot of Detail views. Export the drawing of the full assembly in the desired scale and in the scale of the detail views (it's possible to scale within Inkscape too). [wasn't necessary because the combination of [Part SimpleCopy](https://wiki.freecadweb.org/Part_SimpleCopy) and turn off "Keep updated" for the TechDraw pages until all detail views are set up, worked great!]
   - Increase the use of [Part SimpleCopy](https://wiki.freecadweb.org/Part_SimpleCopy) and [Part compound](https://wiki.freecadweb.org/Part_Compound) to create objects to reference on. This can reduce the effort needed to calculate the TechDraw pages for FreeCAD/the system.
   - Use LibreCAD if the other approaches still take to long to calculate the drawing pages.
3. Think a little bit more about [chamfers](https://wiki.freecadweb.org/PartDesign_Chamfer) and [fillets](https://wiki.freecadweb.org/PartDesign_Fillet) and model them directly as part of the sketch if possible
4. Use the [Macro EasyAlias](https://wiki.freecadweb.org/Macro_EasyAlias) (for Spreadsheets)
5. Use the ["[Macro] Snap Dimension lines to same length / re-center dimension"]([Macro] Snap Dimension lines to same length / re-center dimension)
6. Use [PartDesign Mirrored](https://wiki.freecadweb.org/PartDesign_Mirrored) / [Part Mirror](https://wiki.freecadweb.org/Part_Mirror). I haven't used these tools in this design because I thought that it wouldn't be of much use here. I can now say: I was wrong.
7. Check if using the refine property on the last feature in the tree (per body) might be useful (for example to get rid of the lines on the surface of the part "sidepanel right" where the part "front panel" is going to be mounted). If you don't know what it is, take a look at the [FreeCAD forum](https://forum.freecadweb.org) first and use this feature / setting with care!

Regarding the design / build process:
1. Use high quality sanding paper from the very beginning.
2. Get a vibratory sander from the very beginning.
3. Think about the marks to make clear it which part goes where. The current model shows the marks "as is". Some are redundant.
4. If time allows, finish the 3D-model before actually building the project (that would've helped with point 3).
5. Don't use a random orbital sander. It quickly takes away too much material (especially with a material like multiplex).
6. Think about making the steps deeper, so that you don't have to put two dowels that close to the edge of step 3.

## Things I'd do the same next time
Regarding FreeCAD:
1. Use a [spreadsheet](https://wiki.freecadweb.org/Spreadsheet_Workbench) and [expressions](https://wiki.freecadweb.org/Expressions) to make calculations and keep the design as parametric as possible.
2. Use the [Assembly3 workbench](https://wiki.freecadweb.org/Assembly3_Workbench).
3. Export all drawings to PDF.
4. Export all components to STEP.
5. Make nice drawings using [TechDraw](https://wiki.freecadweb.org/TechDraw_Workbench). :-)

## Possible further developments
I'm not able to work on any of this ideas in the foreseeable future. But this might change at some point in the future and I'd like to have a place to pick my ideas up again. If you're interested in any of these, feel free to contribute!
1. Use this as an example to get into [photorealistic renderings using FreeCAD](https://wiki.freecadweb.org/Tutorial_FreeCAD_POV_ray).
2. Use this as an example to get into [FEM analysis using FreeCAD](https://wiki.freecadweb.org/FEM_Module).
3. Use this as an example to get into the [Animation Workbench](https://wiki.freecadweb.org/Animation_Workbench) using FreeCAD.
4. Create a step by step guide to build this learning tower.
5. Check out the [Fasteners_Workbench](https://wiki.freecadweb.org/Fasteners_Workbench) and add the modeled screw and cross nut bolt to it, if needed / useful.

## Threads (FreeCAD Forum) which helped me regarding this project
1. [[Solved] Apply Part Design fillet to a part of an edge](https://forum.freecadweb.org/viewtopic.php?f=3&t=45903&p=393718#p393718)
2. [[Solved] PartDesign copy body and move features](https://forum.freecadweb.org/viewtopic.php?f=3&t=45931&p=393971#p393971)
3. [[Solved] *Part* mirror - *PartDesign* feature - *PartDesign* modify both PD bodies](https://forum.freecadweb.org/viewtopic.php?f=3&t=46548&p=400259#p400259)
4. [[SOLVED] Asm3 - align pockets with pads](https://forum.freecadweb.org/viewtopic.php?f=20&t=47454&p=407129#p407129)
5. [TechDraw - DetailView - Move Reference](https://forum.freecadweb.org/viewtopic.php?f=35&t=46417&p=399355#p399355)
6. [[SOLVED] Workflow - multiple TechDraw pages for assembly where different parts are visible?](https://forum.freecadweb.org/viewtopic.php?f=3&t=51302&p=440631#p440631)
7. [TechDraw - Create Detail views using Inkscape (for example)](https://forum.freecadweb.org/viewtopic.php?f=3&t=51323&p=440847#p440847)

## Builds that have inspired me
1. [Basteltown: Kinder-Lernturm aus Holz: Einfach selbst gebaut!](https://bastel-town.de/videos/treppen-lernturm-aus-holz-einfach-selbst-gebaut/) (there used to be a video too but it's unavailable right now [checked at 2020-11-09])
2. [Claudio Rivero / Make it now](http://makeitnow.tv/learning_tower/) (detailed blog post and there's a video too)

## Fuckups
Does the previous sound nice and smooth? To be honest, some things went wrong or at least not as planned. I'm convinced that it's important to talk about these things to prevent others and yourself from repeating these mistakes. Dealing with the mistakes made and development potential is an essential part of improvement. Sounds interesting? Have a look at [Aaron Swartz's post on the value of documenting mistakes](http://www.aaronsw.com/weblog/geremiah). I won't go through the whole [5 whys technique](https://en.wikipedia.org/wiki/Five_whys) but I'll make a list of what went wront / not as planned (chronological order).

1. I'd planned to add a radius to the cutouts in the sidepanels and the transitions on the legs of the sidepanels. After I made the cutouts without radius (= as marked), it was too late. However, I like the final result very much and I'd do it like this again.
2. I've checked the base material for damages / repaired damages and decided which side should point inwards or outwards. After making the cutouts for the sidepanels, both parts are interchangeable - until you're starting to make the holes for the sidepanel bars. I've drilled those holes in one side which should have face outwards. Now it's facing inwards.
3. I took the wrong measurement and drilled all holes for the sidepanel bars of one of the sidepanels at the wrong height.
4. I'd planned to move the front panel all the way up to the upper end of the sidepanels. This should prevent children's fingers from being trapped when they hold on to the front panel and the learning tower is pushed towards a worktop or similar. Later I've realized that this doesn't go well with the radius I'd applied to the upper edges of the sidepanels. Therefore the front panel was shortened and moved further down. Now there is again enough space between the front panel and the worktop of our current kitchen.
5. On step 3 I had to place two dowels close to the edge. At one point, the layers of the material separated slightly and had to be glued together.
6. I highly underestimated the time it would take to sand it by hand using low quality sanding paper. I'm now using the [rhynalox plus line from INDASA](https://www.indasa-abrasives.com/global/en/catalogue/abrasives/rhynalox-plus-line) ([this is a source of supply in Germany](https://www.befestigungsfuchs.de)

And keep in mind: “Mistakes are our friend.” (Carol Dweck, Self-Theories: Their Role in Motivation, Personality, and Development (2000), 10.)

## Thank you!
Projects like this wouldn't be possible or a lot harder to realize if the great software I've used so far wouldn't exist / be available. All of it is open source! Thank you very much (alphabetic order):
- [FreeCAD](https://www.freecadweb.org/)
- [GIMP](https://www.gimp.org/)
- [Ubuntu](https://ubuntu.com/)

I hope that I can give something back to the community by sharing my files / this project.

## License
SPDX-FileCopyrightText: 2020 Maximilian Behm <learning_tower@maxtheman.de>\
SPDX-License-Identifier: CERN-OHL-S-2.0+

This source describes Open Hardware and is licensed under the CERN-OHL-S v2
or any later version.

You may redistribute and modify this source and make products using it under
the terms of the CERN-OHL-S v2 or any later version
(https://ohwr.org/cern_ohl_s_v2.txt).

This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.

Source location: https://gitlab.com/M4x__/learning-tower

As per CERN-OHL-S v2 section 4, should You produce hardware based on this
source, You must where practicable maintain the Source Location visible
on the external case and the documentation of the learning tower or other
products you make using this source.

If you redistribute or modify this source or make products using it, I would 
be very happy, if you would send me a note, providing the location of your
published data and ideally a picture of the final product.

I followed the User Guide for the CERN Open Hardware Licence Version 2 -
Strongly Reciprocal (September 2, 2020 / Guide version 1.0). Please have a
look at this guide if you'd like to know more regarding the CERN-OHL
license. It is a very good starting point and a great aid to
decision-making. The guide is part of the distributed source. It can also be
found here:
  - PDF: https://ohwr.org/project/cernohl/wikis/uploads/cf37727497ca2b5295a7ab83a40fcf5a/cern_ohl_s_v2_user_guide.pdf
  - TXT: https://ohwr.org/project/cernohl/wikis/uploads/b88fd806c337866bff655f2506f23d37/cern_ohl_s_v2_user_guide.txt

