# CHANGES
Anyone modifying this design should provide brief information about the modifications, including the date they were made. Please document these changes in this file. Information about the design should be added but never removed from this file. For further information, please have a look at section 3.3b of the license and the suggestions 2.2.4 and 2.2.5 of the license user guide (version 1.0). Example entry:
- 02 December 2020:
   - Increased the total width to 636 mm, because I have twins and they should be to able share the learning tower with enough free space.
---   
- 27 April 2021:
   - published [drw_step_1_RevB_A3.pdf](drawings/components/drw_step_1_RevB_A3.pdf) (minor layout changes)
   - published [learning_tower_drawings_2021-04-27.pdf](drawings/learning_tower_drawings_2021-04-27.pdf)
      - replaced drawing for step 1 Rev. A with Rev. B
      - added bookmarks
   - removed previous versions (drw_step_1_RevA_A3.pdf and learning_tower_drawings_2020-11-09.pdf)
